**_Cephei_**

**Гамма Цефея** — двойная звезда в созвездии Цефея. У арабов соседние звёзды альфа и бета Цефея составляли парный астеризм «Две коровы», с которым ассоциативно и связано указанное название.

Суть проекта:
- простой онлайн конструктор для создания ботов в виде UML диаграмм 
- запуск созданных ботов на сервере системы

Основные модули:
`java`
- domain - модели к базе данных
- web - сервер, базовый модуль 
- bots - имплементация ботов, телеграмм etc
- meta-logic - библиотека для работы с мета языком
- starter - библеотека для запуска ботов
- smart-item - библеотека для умного анализа страниц с бизнесс сущностями
`front`
- front-base - страницы, меню etc, базовый модуль
- front-diagram - библеотека для работы с полотном для uml диаграммм
`other`
- db - настройка, запуск и развертывание базы данных

Методичка разработчику:
Для разработки нового функционалла необходимо создать ветку
git checkout -b feature/%number%
Для фикса бага необходимо создать ветку
git checkout -b bugfix/%number%

где %number% номер issue на gitlab
